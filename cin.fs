vocabulary cin-parser
cin-parser also definitions

: NL ( -- ) 10 ;

: tib-empty? ( -- f )
	>in @ #tib @ >=
;

: # ( -- )
	nl word
\	dup count .s ." got comment: " type cr
	drop
;

: .tagname ( a -- a )
	dup @ ( addr of length )
	dup cell+ ( str )
	swap @ 255 and ( a len )
	." parse " type ." : "
;

: .tagval ( str len -- str len )
	2dup type cr
;

: .flagname ( a -- a )
	.tagname ." true" cr
;

: .pairname ( a -- a )
	." start " .tagname
;

: tag-parser: ( <name> -- )
	here
	create
	cell+ , \ store address of word name
	does> ( <value> -- )
		.tagname
		drop	
		parse-name
		.tagval
		2drop
;

: flag-parser: ( <name> -- )
	here
	create
	cell+ , \ store address of word name
	does> ( -- )
		.flagname
		drop	
;

: parse-pair ( -- )
	\ ." enter: " .s cr
	begin
		\ tib #tib @ dup . >in @ . dump cr
		parse-name ( str len )  \ .s 2dup ." ====== " type cr
		over c@ ( str len str[0] )
		[char] # = if
			evaluate
			tib-empty? if refill invert if exit then then
		else ( str len str[0] )
			\ ." see next word" cr
			over dup c@ swap 1+ c@ ( str len str[0] str[1] )
			bl <> swap [char] % = and if
				\ ." got other tag: " 2dup type cr
				2drop
				0 >in !
				exit
			else
				." parse pair: " type ."  - "
				parse-name .tagval 2drop
				tib-empty? if refill invert if exit then then
			then
		then
	again
	." leave" cr
;

: pair-parser: ( <name> -- )
	here
	create
	cell+ , \ store address of word name
	does> ( -- )
		.pairname
		drop	
		parse-name
		.tagval
		drop
		c@ dup [char] e = if
			." *** end..." cr
			drop
		else
			[char] b = if
				." *** begin..." cr
				tib-empty? if refill invert if exit then then
				parse-pair
			then
		then
;


: %gen_inp ( -- ) ;

tag-parser: %ename
tag-parser: %cname
tag-parser: %selkey
tag-parser: %dupsel
tag-parser: %endkey
tag-parser: %space_style
flag-parser: %keep_key_case
flag-parser: %symbol_kbm
flag-parser: %phase_auto_skip_endkey

pair-parser: %keyname
pair-parser: %chardef

only forth also definitions

